import {
  AuthenticationContext,
  adalFetch,
  withAdalLogin
} from 'react-adal';

export const adalConfig = {
  tenant: '90e8df26-4a4e-44ef-a7f2-9eadb8202682',
  clientId: '1a7f2095-c10f-46b2-8f62-ef83e9402814',
  endpoints: {
    api: '1a7f2095-c10f-46b2-8f62-ef83e9402814',
  },
  redirectUri: "https://localhost:3000/auth-response", // url de redirection
  postLogoutRedirectUri: "https://localhost:3000/logout", // url appelée lors de la déconnexion
  cacheLocation: 'localStorage',
};

export const authContext = new AuthenticationContext(adalConfig);

export const getToken = () => {
  var cachedToken = authContext.getCachedToken(authContext.config.clientId);
  if(cachedToken === undefined) {
    authContext.acquireToken(authContext.config.clientId);
  }
  else {
    return cachedToken;
  }
};

export const adalApiFetch = (fetch, url, options) =>
  adalFetch(authContext, adalConfig.endpoints.api, fetch, url, options);

export const withAdalLoginApi = withAdalLogin(authContext, adalConfig.endpoints.api);
