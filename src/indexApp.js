import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import LogIn from './components/LogIn';
import LogOut from './components/LogOut';
import PageWhenLoggedIn from './components/PageWhenLoggedIn';
import reportWebVitals from './reportWebVitals';
import { withAdalLoginApi } from './adalConfig';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const AuthResponse = withAdalLoginApi(PageWhenLoggedIn, () => <div>Loading...</div>, (error) => <div>{error}</div>)

const Root = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App} ></Route>
      <Route exact path="/login" component={LogIn} ></Route>
      <Route exact path="/logout" component={LogOut} ></Route>
      <Route exact path="/auth-response" render={ ()=> <AuthResponse /> } />
    </Switch>
  </BrowserRouter>
)

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
