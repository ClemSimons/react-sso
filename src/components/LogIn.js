import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import '../LogIn.css';
import { authContext } from '../adalConfig';

const LogIn = () => {

  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [ssoConnexion, setSsoConnexion] = useState(false);

  const handleLoginChange = event => {
    setLogin(event.target.value);
  }

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  }

  const handleConnexion = () => {
  }

  const handleSSOConnexion = () => {
    setSsoConnexion(true);
  }

  if( ssoConnexion === true ){
    return( <Redirect to={"/auth-response"}></Redirect> )
  }

  return(
    <div className='connexionBox' >
      <form className='LogIn' onSubmit={()=> handleConnexion()}>
        <input
          value={login}
          onChange={(event)=> handleLoginChange(event)}
          placeholder='Identifiant'
          type='text'
          required/>
        <input
          value={password}
          onChange={(event)=> handlePasswordChange(event)}
          placeholder='Mot de passe'
          type='text'
          required/>
        <button type='submit'>Connexion</button>
      </form>
      <form className='LogIn'>
        <input
          type='button'
          value='Connexion SSO'
          onClick={()=> handleSSOConnexion()}/>
      </form>
      <form className='LogOut'>
        <input
          type='button'
          value='Log Out SSO'
          onClick={() => authContext.logOut()}/>
      </form>
    </div>
  )
}

export default LogIn;
