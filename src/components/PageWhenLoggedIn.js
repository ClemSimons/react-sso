import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { getToken } from '../adalConfig';

const PageWhenLoggedIn = () => {

  const [getBackToHomePage, setGetBackToHomePage] = useState(false);

  const handleGetBackToHomePage = () => {
    console.log(getToken())
    setGetBackToHomePage(true);
  }

    if( getBackToHomePage === true ){
      return( <Redirect to={"/"}></Redirect> )
    }

    return(
      <div>
        <h1>Logged In!</h1>
        <form>
          <input
            type='button'
            value='Get back to home page'
            onClick={() => handleGetBackToHomePage()}/>
        </form>
      </div>
    )
}

export default PageWhenLoggedIn;
