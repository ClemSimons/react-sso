import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

const LogOut = () => {

  const [getBackToHomePage, setGetBackToHomePage] = useState(false);

  const handleGetBackToHomePage = () => {
    setGetBackToHomePage(true);
  }

  if( getBackToHomePage === true ){
    return( <Redirect to={"/"}></Redirect> )
  }

  return(
    <div>
      <h1>Logged Out!</h1>
      <form>
        <input
          type='button'
          value='Get back to home page'
          onClick={() => handleGetBackToHomePage()}/>
      </form>
    </div>
  )
}

export default LogOut;
